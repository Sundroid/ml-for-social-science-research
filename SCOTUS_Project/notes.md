## General notes about the project:

When analyzing court comments, removing the top 20 terms seemed to do the trick. this might allow to include more topics again. (Trying currently)
Only removing the top10 terms seems to be pointless, as there still are bullshit buzzwords among the "topic-describing-words". I mean academese which is not helpful at all. 

From all those about 10k opinions (overall, not just since 1970), I picked a sample of 1k to make the whole process a lot faster. even with 2.5k opinions, it's unbearably slow (at least on my VM on a laptop), but I guess the size of the sample doesn't matter too much, as using all 10k opinions would just reinforce tendencies which are already shown anyways. 

There are at least two topics, I really couldn't wrap my head around:
- ante id court's decision dissent
- petitioner district_court file 2d respondent
- (and some more)
- thanks for the "decade" attribute in the data, otherwise would have had to figure that out myself...

But I assume, as those are opinions on court decisions, that beginning from about 1910 on, judges made more and more comments about "abuse" or missuse of the law system. 

Also removing even more than the top 20 terms might help. (currently sitting at 25)

Observations: 
- Said topics seem to be a feature of the more recent decades.
- In the earlier decades the opinions seem to be closer to the peoples hearts: the topic concerning bankrupcy, property, and so on seem to decline from the beginning of the period towards the early 1900s.
- in one setting there was a spike for the thematic arount presidents breaking law and missjudgement of presidents and so on, around 1810. This probably is tied to the american-british war. 
- since around the 1950s, cases around working-contracts (worker rights) have increased heavily.
- there are also more opinions about decisions of the congress and federal administration. tied to this there also are more opinions where constituition along with courts are mentioned. I assume, it's about how the law system is built in the US. 
- meanwhile writing opinions around contracts around companies got less interesting. This is accompanied by decreasing comments about the possession of natural ressources. I can imagine, that those are often tied together and then adressed leaning towards one side or the other depending on the judges political attitude
- also from the beginning of the records, comments on the plaintiffs in general have decreased massively. 


all in all: not too happy about the results. topic do not seem to change too much after all, except for the few mentioned ones. Also the results vary heavily depending on number of topics, number of most used words removed and alpha-value in LDA. Also analyzing SCOTUS Opinions doesn't seem to be even close to analyzing SOTU speeches in terms of interestingness

Maybe I should move away from hoping to find any real-world related topics and have a look at law & juristication topics -> this seems to be supported by reading some comments. It is usually the case, that the actual topic of the court-case is a minor side note and the main talk is about process issues, missunderstandings of decisions and so on. 


Final tweaks: 
- changed method on how to remove most used words, (eventhough it presumably does the same thing), settled with 12, as between the 12 & 13th word the biggest gap in occurences was in place 
- Set topic number to 15

