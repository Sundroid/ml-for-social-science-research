#LDA part comes from the tutorial by Gregor Wiedemann and Andreas Niekler https://tm4ss.github.io/

# Optional Task 1 in Taskdescription File. Code majorly taken over from the lab-class   

options(stringsAsFactors = FALSE)
library(quanteda)
require(topicmodels)

library(tidyverse)
#load(url("https://cbail.github.io/Trump_Tweets.Rdata"))
load("TopicModeling/Trump_Tweets.Rdata")

#making a new dataframe from the trumptweets one with the variables needed. 
tmTrump <- cbind.data.frame(trumptweets$status_id, trumptweets$created_at, trumptweets$text, trumptweets$favorite_count, trumptweets$retweet_count)
colnames(tmTrump) <- c("id","date", "text", "favs", "rt's")
#remove urls from text
tmTrump$text <- qdapRegex::rm_twitter_url(tmTrump$text)

#calculating and printing time between first and last tweet in dataset. 
minDate <- min(tmTrump$date)
maxDate <- max(tmTrump$date)
paste(diff_in_days<- difftime(maxDate,minDate , units = c("days")), "days between first and last analyzed tweet. Shame it's only about 1.3 years worth of tweets")

#duplicate
textdata <- tmTrump


#clean up right away
remove(trumptweets)

trump_corpus <- corpus(tmTrump, text_field = "text", docid_field = "id")

#we are going to use lemmatization
# so we build a dictionary of lemmas (import it)
lemma_data <- read.csv("TopicModeling/baseform_en.tsv", encoding = "UTF-8")
# we also import an extended stopword list
#though there is a native stopwords list in quanteda and other packages (e.g. tm package), we have a more comprehensive one here, so we import it
#might be a less of a problem with English (the native lists are quite good), but if you go for other languages, it might be better to find dedicated stopwords lists developed, e.g. by Universities in country N, and import them
stopwords_extended <- readLines("TopicModeling/stopwords_en.txt", encoding = "UTF-8")


#here we also tokenize texts and remove punctuation, numbers and special symbols
#then we bring our tokens to lower case
#and lemmatize them
#finally, we remove tokens from the stopwords list [leaving empty strings where the removed tokens used to be since padding=TRUE]
corpus_tokens <- trump_corpus %>% 
  tokens(remove_punct = TRUE, remove_numbers = TRUE, remove_symbols = TRUE) %>% 
  tokens_tolower() %>% 
  tokens_replace(lemma_data$inflected_form, lemma_data$lemma, valuetype = "fixed") %>% 
  tokens_remove(pattern = stopwords_extended, padding = T)

# #the next step is optional --> And for tweets most likely not applicable as they are max 280 Chars long
# #now we also want to take into account frequently co-occurring tokens and see them as one
# #we find the collocations
trump_collocations <- textstat_collocations(corpus_tokens, min_count = 25)
# #select the top 250 (number is rather arbitrary)
trump_collocations <- trump_collocations[1:250, ]
# #and replace single tokens from these collocations with the compound ones
# corpus_tokens <- tokens_compound(corpus_tokens, trump_collocations)


# Model calculation

#from our preprocessed corpus (with cleaned tokens etc - see above) we create a DTM (dfm)
# in addition, we remove terms which occur in less than 1% of all documents
DTM <- corpus_tokens %>% 
  tokens_remove("") %>%
  dfm() %>% 
  dfm_trim(min_docfreq = 0.01, max_docfreq = Inf, docfreq_type = "prop")
# have a look at the number of documents and terms in the matrix
dim(DTM)


# #For topic modeling not only language specific stop words may beconsidered as uninformative, but also domain specific terms. We remove 10 of the most frequent terms to improve the modeling.
# # Again: Left because tweets are too short to name too many arbitrary words. 
# top10_terms <- c( "unite_state", "past_year", "year_ago", "year_end", "government", "state", "country", "year", "make", "seek")
# DTM <- DTM[, !(colnames(DTM) %in% top10_terms)]

# due to vocabulary pruning, we have empty rows in our DTM
# LDA does not like this. So we remove those docs from the
# DTM and the metadata
sel_idx <- rowSums(DTM) > 0
DTM <- DTM[sel_idx, ]
textdata <- textdata[sel_idx, ]


#As an unsupervised machine learning method, topic models are suitable for the exploration of data. The calculation of topic models aims to determine the proportionate composition of a fixed number of topics in the documents of a collection. It is useful to experiment with different parameters in order to find the most suitable parameters for your own analysis needs.

#For parameterized models such as Latent Dirichlet Allocation (LDA), the number of topics `K` is the most important parameter to define in advance. How an optimal `K` should be selected depends on various factors. If `K` is too small, the collection is divided into a few very general semantic contexts. If `K` is too large, the collection is divided into too many topics of which some may overlap and others are hardly interpretable.

#For our first analysis we choose a thematic "resolution" of `K = 20` topics. In contrast to a resolution of 100 or more, this number of topics can be evaluated qualitatively very easy.



require(topicmodels)
# number of topics
K <- 20
# set random number generator seed
set.seed(9161)
# compute the LDA model, inference via 500 iterations of Gibbs sampling
#we use Gibbs sampling to fit the model (500 iterations), to make sure it's stable. Verbose >0 is for logging (print out the progress)
topicModel <- LDA(DTM, K, method="Gibbs", control=list(iter = 500, verbose=25))


#Depending on the size of the vocabulary, the collection size and the number K, the inference of topic models can take a very long time. This calculation may take several minutes. If it takes too long, reduce the vocabulary in the DTM by increasing the minimum frequency in the previous step.

#The topic model inference results in two (approximate) posterior probability distributions: a distribution `theta` over K topics within each document and a distribution `beta` over V terms within each topic, where V represents the length of the vocabulary of the collection (V = `r ncol(DTM)`). Let's take a closer look at these results:



# have a look a some of the results (posterior distributions)
tmResult <- posterior(topicModel)
# format of the resulting object
attributes(tmResult)
ncol(DTM)                # lengthOfVocab
# topics are probability distribtions over the entire vocabulary
beta <- tmResult$terms   # get beta from results
dim(beta)                # K distributions over ncol(DTM) terms
rowSums(beta)            # rows in beta sum to 1
nrow(DTM)               # size of collection
# for every document we have a probability distribution of its contained topics
theta <- tmResult$topics 
dim(theta)               # nDocs(DTM) distributions over K topics
rowSums(theta)[1:10]     # rows in theta sum to 1


#Let's take a look at the 10 most likely terms within the term probabilities `beta` of the inferred topics.
terms(topicModel, 10)


#we can save the top terms, say, for the first 8 topics
exampleTermData <- terms(topicModel, 10)
exampleTermData[, 1:8]


#For the next steps, we want to give the topics more descriptive names than just numbers. Therefore, we simply concatenate the five most likely terms of each topic to a string that represents a pseudo-name for each topic.

top5termsPerTopic <- terms(topicModel, 5)
topicNames <- apply(top5termsPerTopic, 2, paste, collapse=" ")

# Topic distributions

#The figure above shows how topics within a document are distributed according to the model. In the current model all three documents show at least a small percentage of each topic. However, two to three topics dominate each document.

#The topic distribution within a document can be controlled with the *Alpha*-parameter of the model. Higher alpha priors for topics result in an even distribution of topics within a document. Low alpha priors ensure that the inference process distributes the probability mass on a few topics for each document. 

#In the previous model calculation the alpha-prior was automatically estimated in order to fit to the data (highest overall probability of the model). However, this automatic estimate does not necessarily correspond to the results that one would like to have as an analyst. Depending on our analysis interest, we might be interested in a more peaky/more even distribution of topics in the model. 

#Now let us change the alpha prior to a lower value to see how this affects the topic distributions in the model.


# see alpha from previous model
attr(topicModel, "alpha") 

topicModel2 <- LDA(DTM, K, method="Gibbs", control=list(iter = 500, verbose = 25, alpha = 0.2))
tmResult <- posterior(topicModel2)
theta <- tmResult$topics
beta <- tmResult$terms
topicNames <- apply(terms(topicModel2, 5), 2, paste, collapse = " ")  # reset topicnames

# Topic ranking (Main Task)

#First, we try to get a more meaningful order of top terms per topic by re-ranking them with a specific score @Chang.2012. The idea of re-ranking terms is similar to the idea of TF-IDF. The more a term appears in top levels w.r.t. its probability, the less meaningful it is to describe the topic. Hence, the scoring favors less general, more specific terms to describe a topic.

#you need lda package for this
# re-rank top topic terms for topic names
topicNames <- apply(lda::top.topic.words(beta, 5, by.score = T), 2, paste, collapse = " ")


#What are the defining topics within a collection? There are different approaches to find out which can be used to bring the topics into a certain order.

#Approach 1: We sort topics according to their probability within the entire collection:


# What are the most probable topics in the entire collection?
topicProportions <- colSums(theta) / nrow(DTM)  # mean probablities over all paragraphs
names(topicProportions) <- topicNames     # assign the topic names we created before
sort(topicProportions, decreasing = TRUE) # show summed proportions in decreased order

soP <- sort(topicProportions, decreasing = TRUE)
paste(round(soP, 5), ":", names(soP))


#We recognize some topics that are way more likely to occur in the corpus than others. These describe rather general thematic coherences. Other topics correspond more to specific contents. 

#Approach 2: We count how often a topic appears as a primary topic within a paragraph This method is also called Rank-1.


countsOfPrimaryTopics <- rep(0, K)
names(countsOfPrimaryTopics) <- topicNames
for (i in 1:nrow(DTM)) {
topicsPerDoc <- theta[i, ] # select topic distribution for document i
# get first element position from ordered list
primaryTopic <- order(topicsPerDoc, decreasing = TRUE)[1] 
countsOfPrimaryTopics[primaryTopic] <- countsOfPrimaryTopics[primaryTopic] + 1
}
sort(countsOfPrimaryTopics, decreasing = TRUE)

so <- sort(countsOfPrimaryTopics, decreasing = TRUE)
paste(so, ":", names(so))

# Topic proportions over time

#In a last step with LDA, we provide a distant view on the topics in the data over time. For this, we aggregate mean topic proportions per decade of all SOTU speeches. These aggregated topic proportions can then be visualized, e.g. as a bar plot. 


# append decade information for aggregation
textdata$month <- substr(textdata$date, 0, 7)
# get mean topic proportions per decade
topic_proportion_per_month <- aggregate(theta, by = list(month = textdata$month), mean)
# set topic names to aggregated columns
colnames(topic_proportion_per_month)[2:(K+1)] <- topicNames
# reshape data frame
vizDataFrame <- melt(topic_proportion_per_month, id.vars = "month")
# plot topic proportions per decade as bar plot
#we need package pals
require(pals)
ggplot(vizDataFrame, aes(x=month, y=value, fill=variable)) + 
geom_bar(stat = "identity") + ylab("proportion") + 
scale_fill_manual(values = paste0(alphabet(20), "FF"), name = "topic") + 
theme(axis.text.x = element_text(angle = 90, hjust = 1))


#The visualization shows that topics around the relation between the federal government and the states as well as inner conflicts clearly dominate the first decades. Security issues and the economy are the most important topics of recent SOTU addresses.